<div align="center">

<h1> <a href="https://phosphorous.gitlab.io/demeter">
<img src="icon.svg" height="64"> Demeter
</a> </h1>

[![Latest version](https://img.shields.io/maven-metadata/v?metadataUrl=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F37936830%2Fpackages%2Fmaven%2Fcom%2Fgitlab%2Faecsocket%2Fdemeter%2Fdemeter-core%2Fmaven-metadata.xml)](https://gitlab.com/phosphorous/demeter/-/packages/TODO)
[![Pipeline status](https://img.shields.io/gitlab/pipeline-status/phosphorous/demeter?branch=main)](https://gitlab.com/phosphorous/demeter/-/pipelines/latest)

</div>

Climate, weather, seasonal and environmental effects

### [Quickstart and documentation](https://phosphorous.gitlab.io/demeter)

## Downloads for v0.1.0

### [Paper](https://gitlab.com/api/v4/projects/37936830/jobs/artifacts/main/raw/paper/build/libs/demeter-paper-0.1.0.jar?job=build)
### [Paper (mojmap)](https://gitlab.com/api/v4/projects/37936830/jobs/artifacts/main/raw/paper/build/libs/demeter-paper-0.1.0-dev-all.jar?job=build)
