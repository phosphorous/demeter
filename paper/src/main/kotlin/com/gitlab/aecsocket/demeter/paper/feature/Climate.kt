package com.gitlab.aecsocket.demeter.paper.feature

import com.gitlab.aecsocket.alexandria.core.RangeMapDouble
import com.gitlab.aecsocket.demeter.paper.Demeter
import com.gitlab.aecsocket.demeter.paper.DemeterFeature
import org.bukkit.World
import org.spongepowered.configurate.objectmapping.ConfigSerializable
import kotlin.math.PI
import kotlin.math.sin

private fun noise(x: Double) = sin(2*x) + sin(PI*x)

data class ClimateInfo(
    val temperature: Double
) {
    operator fun plus(i: ClimateInfo) = ClimateInfo(temperature + i.temperature)

    companion object {
        val Zero = ClimateInfo(0.0)
    }
}

class Climate(
    private val demeter: Demeter
) : DemeterFeature {
    @ConfigSerializable
    data class Settings(
        val byTime: RangeMapDouble = RangeMapDouble.Zero,
        val byTimeScale: Double = 0.0,
        val bySunHeight: RangeMapDouble = RangeMapDouble.Zero
    )

    lateinit var worldFeatures: Demeter.WorldFeatures private set
    lateinit var world: World private set
    lateinit var settings: Settings private set

    private var seed = 0.0

    override fun setup(features: Demeter.WorldFeatures) {
        worldFeatures = features
        world = worldFeatures.world
        seed = world.seed % 0x10000.toDouble()
    }

    override fun load() {
        settings = demeter.settings.worlds[world].climate
    }

    fun byTime() = ClimateInfo(
        settings.byTime.map(noise(seed + worldFeatures.dayCycle.days * settings.byTimeScale))
    )

    fun bySunHeight() = ClimateInfo(
        settings.bySunHeight.map(worldFeatures.dayCycle.sunHeight())
    )

    fun base(): ClimateInfo {
        return byTime() + bySunHeight()
    }

    fun at(x: Int, y: Int, z: Int): ClimateInfo {
        return base()
    }
}
