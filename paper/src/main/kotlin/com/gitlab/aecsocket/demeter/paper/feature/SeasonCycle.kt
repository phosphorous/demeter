package com.gitlab.aecsocket.demeter.paper.feature

import com.gitlab.aecsocket.demeter.paper.Demeter
import com.gitlab.aecsocket.demeter.paper.DemeterFeature
import org.bukkit.World
import org.spongepowered.configurate.objectmapping.ConfigSerializable
import org.spongepowered.configurate.objectmapping.meta.Required
import org.spongepowered.configurate.objectmapping.meta.Setting

class SeasonCycle(
    private val demeter: Demeter
) : DemeterFeature {
    @ConfigSerializable
    data class Settings(
        @Setting(nodeFromParent = true) val seasons: List<Season> = emptyList()
    )

    @ConfigSerializable
    data class Season(
        @Required val id: String,
        @Required val duration: Double, // in days
        val temperature: Double
    )

    lateinit var worldFeatures: Demeter.WorldFeatures private set
    lateinit var world: World private set
    lateinit var settings: Settings private set

    override fun setup(features: Demeter.WorldFeatures) {
        worldFeatures = features
        world = worldFeatures.world
    }

    override fun load() {
        settings = demeter.settings.worlds[world].seasonCycle
    }
}
