package com.gitlab.aecsocket.demeter.paper.feature

import com.github.retrooper.packetevents.event.PacketSendEvent
import com.github.retrooper.packetevents.wrapper.play.server.WrapperPlayServerTimeUpdate
import com.gitlab.aecsocket.alexandria.core.extension.TPS
import com.gitlab.aecsocket.alexandria.paper.sendPacket
import com.gitlab.aecsocket.demeter.paper.Demeter
import com.gitlab.aecsocket.demeter.paper.DemeterFeature
import org.bukkit.World
import org.bukkit.entity.Player
import org.spongepowered.configurate.objectmapping.ConfigSerializable
import java.time.Duration
import kotlin.math.PI
import kotlin.math.sin

private const val DAY_DURATION_TICKS = 20 * 60 * TPS

// 0.0  -> 06:00 (0)
// 0.25 -> 12:00 (6000)
// 0.5  -> 18:00 (12000)
// 0.75 -> 00:00 (18000)
// 1.0  -> 06:00 (24000)
private fun dayTimeFor(progress: Double): Long = (progress * DAY_DURATION_TICKS).toLong()

class DayCycle(
    private val demeter: Demeter
) : DemeterFeature {
    @ConfigSerializable
    data class Settings(
        val dayDuration: Duration = Duration.ofMinutes(20),
    )

    lateinit var worldFeatures: Demeter.WorldFeatures private set
    lateinit var world: World private set
    lateinit var settings: Settings private set

    private var dayFactor = 1.0

    var days = 0.0
        private set
    var dayTime = 0.0
        private set

    override fun setup(features: Demeter.WorldFeatures) {
        worldFeatures = features
        world = worldFeatures.world
    }

    override fun load() {
        settings = demeter.settings.worlds[world].dayCycle
        dayFactor = (settings.dayDuration.seconds * TPS).toDouble()
    }

    override fun update() {
        days = world.fullTime.toDouble() / dayFactor
        dayTime = days % 1.0
        world.players.forEach { player ->
            player.sendPacket(WrapperPlayServerTimeUpdate(world.gameTime, 0L /* to be modified later */))
        }
    }

    override fun updateTime(player: Player, packet: WrapperPlayServerTimeUpdate, event: PacketSendEvent) {
        packet.timeOfDay = dayTimeFor(dayTime)
    }

    fun sunHeight() = sin(2 * PI * dayTime)
}
