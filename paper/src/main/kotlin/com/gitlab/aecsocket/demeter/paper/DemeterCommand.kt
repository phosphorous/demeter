package com.gitlab.aecsocket.demeter.paper

import cloud.commandframework.bukkit.parsers.WorldArgument
import cloud.commandframework.bukkit.parsers.location.LocationArgument
import com.gitlab.aecsocket.alexandria.core.extension.point
import com.gitlab.aecsocket.alexandria.paper.BaseCommand
import com.gitlab.aecsocket.alexandria.paper.Context
import com.gitlab.aecsocket.alexandria.paper.desc
import com.gitlab.aecsocket.alexandria.paper.extension.position
import com.gitlab.aecsocket.glossa.core.I18N
import net.kyori.adventure.text.Component
import org.bukkit.Location
import org.bukkit.World
import org.bukkit.command.CommandSender

internal class DemeterCommand(
    override val plugin: Demeter
) : BaseCommand(plugin) {
    init {
        val rootIn = root
            .literal("in", desc("Select the world to run a command in."))
            .argument(WorldArgument.of("world"), desc("World to run a command in."))

        val climate = rootIn
            .literal("climate", desc("Access the climate state."))

        manager.command(climate
            .literal("base", desc("Read the climate state of the world."))
            .permission(perm("in.climate.base"))
            .handler { handle(it, ::inClimateBase) })
        manager.command(climate
            .literal("at", desc("Read the climate state at a position."))
            .argument(LocationArgument.of("position"), desc("The position."))
            .permission(perm("in.climate.at"))
            .handler { handle(it, ::inClimateAt) })
    }

    fun inClimateBase(ctx: Context, sender: CommandSender, i18n: I18N<Component>) {
        val world = ctx.get<World>("world")

        val climate = plugin.featuresOf(world).climate
        sender.sendMessage("byTime = ${climate.byTime()}")
        sender.sendMessage("bySunHeight = ${climate.bySunHeight()}")
        sender.sendMessage("= ${climate.base()}")
    }

    fun inClimateAt(ctx: Context, sender: CommandSender, i18n: I18N<Component>) {
        val world = ctx.get<World>("world")
        val position = ctx.get<Location>("position")

        val (x, y, z) = position.position().point()
        val climateInfo = plugin.featuresOf(world).climate.at(x, y, z)
        sender.sendMessage("$climateInfo")
    }
}
