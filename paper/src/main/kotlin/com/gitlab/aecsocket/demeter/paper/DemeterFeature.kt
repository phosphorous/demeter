package com.gitlab.aecsocket.demeter.paper

import com.github.retrooper.packetevents.event.PacketSendEvent
import com.github.retrooper.packetevents.wrapper.play.server.WrapperPlayServerTimeUpdate
import org.bukkit.entity.Player

interface DemeterFeature {
    fun setup(features: Demeter.WorldFeatures)

    fun load() {}

    fun dispose() {}

    fun update() {}

    fun updateTime(player: Player, packet: WrapperPlayServerTimeUpdate, event: PacketSendEvent) {}
}
