package com.gitlab.aecsocket.demeter.paper

import org.bukkit.World
import org.spongepowered.configurate.ConfigurationNode
import org.spongepowered.configurate.serialize.SerializationException
import org.spongepowered.configurate.serialize.TypeSerializer
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

private const val DEFAULT = "default"

class PerWorld<T>(
    private val default: T,
    private val byName: Map<String, T> = emptyMap(),
) {
    private val byWorld = HashMap<World, T>()

    operator fun get(world: World): T {
        return byWorld.computeIfAbsent(world) { byName[world.name] ?: default }
    }

    object Serializer : TypeSerializer<PerWorld<*>> {
        override fun serialize(type: Type, obj: PerWorld<*>?, node: ConfigurationNode) {}

        override fun deserialize(type: Type, node: ConfigurationNode): PerWorld<*> {
            if (type !is ParameterizedType)
                throw SerializationException(node, type, "Raw types are not supported for PerWorlds")
            if (type.actualTypeArguments.size != 1)
                throw SerializationException(node, type, "Quantifier expected one type argument")

            val valueType = type.actualTypeArguments[0]
            val valueSerial = node.options().serializers()[valueType]
                ?: throw SerializationException(node, type, "No type serializer available for value type $valueType")

            val default = valueSerial.deserialize(valueType, node.node(DEFAULT))

            return PerWorld(default, node.childrenMap()
                .filter { (key) -> key != DEFAULT }
                .map { (key, value) -> key.toString() to valueSerial.deserialize(valueType, value) }
                .associate { it }
            )
        }
    }
}
