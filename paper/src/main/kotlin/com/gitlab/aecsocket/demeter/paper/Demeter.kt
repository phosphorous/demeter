package com.gitlab.aecsocket.demeter.paper

import com.github.retrooper.packetevents.PacketEvents
import com.github.retrooper.packetevents.event.PacketListenerAbstract
import com.github.retrooper.packetevents.event.PacketSendEvent
import com.github.retrooper.packetevents.protocol.packettype.PacketType
import com.github.retrooper.packetevents.wrapper.play.server.WrapperPlayServerTimeUpdate
import com.gitlab.aecsocket.alexandria.core.LogList
import com.gitlab.aecsocket.alexandria.core.extension.register
import com.gitlab.aecsocket.alexandria.paper.AlexandriaAPI
import com.gitlab.aecsocket.alexandria.paper.BasePlugin
import com.gitlab.aecsocket.alexandria.paper.PluginManifest
import com.gitlab.aecsocket.alexandria.paper.extension.registerEvents
import com.gitlab.aecsocket.alexandria.paper.extension.scheduleRepeating
import com.gitlab.aecsocket.demeter.paper.feature.Climate
import com.gitlab.aecsocket.demeter.paper.feature.SeasonCycle
import com.gitlab.aecsocket.demeter.paper.feature.DayCycle
import com.gitlab.aecsocket.glossa.core.force
import net.kyori.adventure.text.format.TextColor
import org.bstats.bukkit.Metrics
import org.bukkit.Bukkit
import org.bukkit.World
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.world.WorldUnloadEvent
import org.spongepowered.configurate.ConfigurationNode
import org.spongepowered.configurate.objectmapping.ConfigSerializable

private const val BSTATS_ID = 13021

private lateinit var instance: Demeter
val DemeterAPI get() = instance

class Demeter : BasePlugin(PluginManifest("demeter",
    accentColor = TextColor.color(0x949ede),
    langPaths = listOf(
        "lang/default_en-US.conf",
        "lang/en-US.conf"
    ),
    savedPaths = listOf(
        "settings.conf",
        "lang/en-US.conf"
    )
)) {
    @ConfigSerializable
    data class Settings(
        val enableBstats: Boolean = true,
        val worlds: PerWorld<WorldSettings> = PerWorld(WorldSettings()),
    )

    @ConfigSerializable
    data class WorldSettings(
        val climate: Climate.Settings = Climate.Settings(),
        val dayCycle: DayCycle.Settings = DayCycle.Settings(),
        val seasonCycle: SeasonCycle.Settings = SeasonCycle.Settings(),
    )

    class WorldFeatures(
        val world: World,
        val climate: Climate,
        val dayCycle: DayCycle,
        val seasonCycle: SeasonCycle,
    ) {
        val all = listOf(dayCycle, seasonCycle, climate)

        init {
            all.forEach { it.setup(this) }
        }

        internal fun run(action: (DemeterFeature) -> Unit) {
            all.forEach(action)
        }
    }

    lateinit var settings: Settings private set
    private val worlds = HashMap<World, WorldFeatures>()

    init {
        instance = this
    }

    override fun onEnable() {
        super.onEnable()
        DemeterCommand(this)
        AlexandriaAPI.registerConsumer(this,
            onInit = {
                serializers.register(PerWorld.Serializer)
            },
            onLoad = {
                addDefaultI18N()
            })

        registerEvents(object : Listener {
            @EventHandler
            fun on(event: WorldUnloadEvent) {
                worlds.remove(event.world)?.run { it.dispose() }
            }
        })

        PacketEvents.getAPI().eventManager.registerListener(object : PacketListenerAbstract() {
            override fun onPacketSend(event: PacketSendEvent) {
                val player = event.player as? Player ?: return
                when (event.packetType) {
                    PacketType.Play.Server.TIME_UPDATE -> {
                        val packet = WrapperPlayServerTimeUpdate(event)
                        featuresOf(player.world).run { it.updateTime(player, packet, event) }
                    }
                }
            }
        })

        scheduleRepeating {
            Bukkit.getWorlds().forEach { world ->
                featuresOf(world).run { it.update() }
            }
        }
    }

    override fun loadInternal(log: LogList, config: ConfigurationNode): Boolean {
        if (!super.loadInternal(log, config)) return false
        settings = config.force()

        // bStats
        if (settings.enableBstats) {
            Metrics(this, BSTATS_ID)
        }

        worlds.forEach { (_, features) -> features.run { it.load() } }

        return true
    }

    override fun onDisable() {
        worlds.forEach { (_, features) -> features.run { it.dispose() } }
    }

    private fun createFeatures(world: World) = WorldFeatures(
        world,
        Climate(this),
        DayCycle(this),
        SeasonCycle(this),
    ).also { features -> features.run { it.load() } }

    fun featuresOf(world: World) = worlds.computeIfAbsent(world) { createFeatures(it) }
}
